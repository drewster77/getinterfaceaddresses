//
//  ViewController.swift
//  GetInterfaceAddresses
//
//  Created by Andrew Benson on 9/13/19.
//  Copyright © 2019 Nuclear Cyborg Corp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let foobar = getIFAddresses()
        for info in foobar {
            print(info)
        }
    }


    struct NetworkInterfaceInfo: CustomStringConvertible {
        let interface: String
        let ip: String
        let netmask: String
        let isUp: Bool
        let isRunning: Bool
        let isLoopback: Bool
        let isIpV4: Bool
        let isIpV6: Bool

        public var description: String {
            var flags: [String] = [ isUp ? "UP" : "DOWN" ]
            if isRunning { flags.append("RUNNING") }
            if isLoopback { flags.append("LOOPBACK") }
            if isIpV4 { flags.append("IPV4") }
            if isIpV6 { flags.append("IPV6") }
            let flagString = flags.joined(separator: " ")
            return "\(interface) (\(flagString)): ip \(ip), mask \(netmask)"
        }
    }

    func getIFAddresses() -> [NetworkInterfaceInfo] {
        var interfaces = [NetworkInterfaceInfo]()
        //let d0 = NSDate()
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {

            // For each interface ...
            var ptr = ifaddr
            while( ptr != nil) {

                let flags = Int32(ptr!.pointee.ifa_flags)
                var addr = ptr!.pointee.ifa_addr.pointee

                let isUp = flags & IFF_UP != 0
                let isRunning = flags & IFF_RUNNING != 0
                let isLoopback = flags & IFF_LOOPBACK != 0
                let isIpV4 = addr.sa_family == UInt8(AF_INET)
                let isIpV6 = addr.sa_family == UInt8(AF_INET6)

                // Convert interface address to a human readable string:
                let zero  = CChar(0)
                var hostname = [CChar](repeating: zero, count: Int(NI_MAXHOST))
                var netmask =  [CChar](repeating: zero, count: Int(NI_MAXHOST))

                var address = "<error>"
                var ifName = "<error>"
                var netmaskIP = "<error>"

                if (getnameinfo(&addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                    address = String(cString: hostname)
                    let name = ptr!.pointee.ifa_name!
                    ifName = String(cString: name)

                    if var mask = ptr!.pointee.ifa_netmask?.pointee {
                        if (getnameinfo(&mask, socklen_t(mask.sa_len), &netmask, socklen_t(netmask.count),
                                        nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                            netmaskIP = String(cString: netmask)
                        }
                    }
                }

                let info = NetworkInterfaceInfo(interface: ifName,
                                                ip: address,
                                                netmask: netmaskIP,
                                                isUp: isUp,
                                                isRunning: isRunning,
                                                isLoopback: isLoopback,
                                                isIpV4: isIpV4,
                                                isIpV6: isIpV6)
                interfaces.append(info)

                // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
//                if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
//                    if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
//
//
//
//                    }
//                } else {
//                    print("Interface not up or running")
//                }
                ptr = ptr!.pointee.ifa_next
            }
            freeifaddrs(ifaddr)
        } else {
            print("Couldn't get any interface addresses.")
        }
        return interfaces
    }
}

